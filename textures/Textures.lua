--add your custom textures here
--ensure that these textures have 64x64 as dimensions
DAoCBuff.textures.Custom=
{
	
}

--____________________Do not edit below this Line!!!____________________
DAoCBuff.textures.DAoC_New=
{
	"DAoC_new_qm",
	"DAoC_new_disease1",
	"DAoC_new_hot1",
	"DAoC_new_root1",
	"DAoC_new_snare1",
	"DAoC_new_stun1",
	"DAoC_new_stun_immunity1",
	"DAoC_new_dot1",
	"DAoC_new_dot2",
	"DAoC_new_str-con",
	"DAoC_new_clean1",
}
DAoCBuff.textures.DAoC_Old=
{
	"DAoC_old_disease1",
	"DAoC_old_hot1",
	"DAoC_old_root1",
	"DAoC_old_snare1",
	"DAoC_old_stun1",
	"DAoC_old_stun_immunity1",
	"DAoC_old_dot1",
	"DAoC_old_dot2",
	"DAoC_old_str-con",
	"DAoC_old_clean1",
}
DAoCBuff.textures.Ycons=
{
	"Yakar_healbuff",
	"Yakar_healdebuff50",
	"Yakar_healdebuff25",
	"Yakar_healoutdebuff",
	"Yakar_silence",
	"Yakar_silence!!!",
	"Yakar_disarm",
	"Yakar_root",
	"Yakar_snare",
	"Yakar_hot",
	"Yakar_dot",
	"Yakar_detaunt",
	"Yakar_curable",
	"Yakar_statsbuff",
	"Yakar_statsdebuff",
	"Yakar_battlemaster",
	"Yakar_lock_gifts",
}
